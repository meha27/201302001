class DoublyLinkedList(object):
	class Node(object):
		def __init__(self,key,val,nxt=None,prev=None):
			self.a=key
			self.b=val
			

		def __eq__(self,key):
			if self.a == key:
			   return True
			else:
			   return False

		def val(self):
			return self.b

        def __init__(self):
			self.head = None
			self.tail = None

	def length(self):
			len=0                                       #to calculate the number of nodes in the linked list"""
			t=self.head
			if t==None:
			   return 0
			else:
			   while t!=None:                          # """traverses the list node by node untill none is reached"""
			        len=len+1
			        t=t.nxt
			   return len

	def insert(self,key,val):
			if(key!=None and val!=None):
				new = DoublyLinkedList.Node(key,val)
				new.a=key
				new.b=val                               # """inserts in the beginning of the list"""
				new.nxt=self.head                        # """head is now the new node which is inserted"""
				t=self.head
				if t!=None:
				  t.prev=new
				self.head=new
			else:
			        return None
				

	def find(self,key):
			t=0
			k=self.head
			while k!=None:
			  if k.a==key:                                    # """ finds the corresponding key value"""
			     t=1                                  # """the one at the rear of the list is returned"""
			     break
			  else:
			     k=k.nxt
			if t==1:
			   return k.b
			elif t==0:
			   return None

	def deleteLast(self):
		        
			if self.head == None:             # """tail is passed"""
			   return                         # """last node i.e the tail is deleted"""
			if self.length()==1:          # """second last node is set as the new tail"""
			   self.head=self.tail=None
			   return
			else:
			   k=self.head.nxt
			   k.prev=None
			   self.head=k


			       
	def deleteFirst(self):
			t=self.head
			if self==None:              #"""head node is deleted"""
			   return                   #"""second last node is set as new head"""
			else:
			   k=t.nxt
			   t.nxt=None
			   self.head=k
			return

	def delete(self,key):
			k=self.head
			while k!=None:
			 if k.a==key:           # """node wid the corresponding key element is searched and then deleted"""
			     
			   
			      k=k.nxt
			      t=k.prev
			      l=k.nxt
			      t.nxt=l
			      k.nxt=None
			      k.prev=None
			      return
			 else:
			      continue
			 print "not found"     






def tests():
	 def testEmptyList():
		 l=DoublyLinkedList()                  # """ """ an empty list is created""" """
		 assert(l.length()==0)      # """ """ since no insertions r made the length is zero""" """

	 def testone():
		 l=DoublyLinkedList()
		 l.insert("name","meha")
		 assert(l.find("name")=="meha")
		 l.deleteFirst()
		 assert(l.length()==0)

	 def testList():
		l=DoublyLinkedList()
		l.insert("name","rashi")
		assert(l.length()==1)
	        l.insert("name","roopal")
                l.insert("name","neil")
	        l.insert("name","niki")
		l.insert("name","vatika")
                assert(l.length()==5)          # """ """ since 5 insertions are made that is why length is 5""" """
                l.deleteFirst()
                l.deleteFirst()                     # """  """deletion from the beginning""" """
                assert(l.length()==3)
                assert(l.find("name")=="vatika")
                l.deleteFirst()
	        l.deleteFirst()
	        assert(l.length()==1)
		l.deleteFirst()
		
         def testDel():                               # """  """ checks deletion from the end""" """
 
	         l=DoublyLinkedList()
	         l.insert("name","ssd")
	         l.insert("name","dpfb")
	         l.insert("name","sbhgy")
	         assert(l.length()==3)       # """ """ length of the list is 3 as 3 insertions are made""" """ 
	         l.deleteLast()
                 l.deleteLast()
	         l.deleteLast()
	         assert(l.length()==0)

