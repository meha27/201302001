#!/usr/bin/python

def length(tuplev):
	lena=tuplev[0][0]-tuplev[1][0]
	lenb=tuplev[0][1]-tuplev[1][1]
	lenc=tuplev[0][2]-tuplev[1][2]
	x=lena*lena+lenb*lenb+lenc*lenc
	return pow(x,0.5)
	


def normalize(tuplev):
        a=tuplev[0][0]-tuplev[1][0]
        b=tuplev[0][1]-tuplev[1][1]
        c=tuplev[0][2]-tuplev[1][2]
        x=(a/length(tuplev),b/length(tuplev),c/length(tuplev))
	return ((0,0,0),x)


def dot_product(tuple1,tuple2):
	a=tuple1[1][0]-tuple1[0][0]
	b=tuple1[1][1]-tuple1[0][1]
	c=tuple1[1][2]-tuple1[0][2]
	x=tuple2[1][0]-tuple2[0][0]
	y=tuple2[1][1]-tuple2[0][1]
	z=tuple2[1][2]-tuple2[0][2]
	return a*x+b*y+c*z


def cross_product(tuple1,tuple2):
	 x=tuple1[1][0]-tuple1[0][0]
	 y=tuple1[1][1]-tuple1[0][1]
	 z=tuple1[1][2]-tuple1[0][2]
	 a=tuple2[1][0]-tuple2[0][0]
	 b=tuple2[1][1]-tuple2[0][1]
	 c=tuple2[1][2]-tuple2[0][2]
	 return ((0,0,0),((y*c-b*z),(a*z-x*c),(x*b-y*a)))

"""if __name__=="__main__":

assert(length(((0,0,0),(3,4,0)))==5.0)
assert(dot_product(((0,0,0),(3,4,0)),((1,2,0),(2,5,1)))==15)	
assert(cross_product(((0,0,0),(3,4,0)),((1,2,0),(2,5,1)))==((0, 0, 0), (4, -3, 5)))
assert(normalize(((1,2,3),(4,6,3)))==((0, 0, 0), (-0.6, -0.8, 0.0)))
"""




    
