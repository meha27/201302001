import q1

"""Users={}
 Users[201301203]=("Rashi","Hyderabad","20.02")
 Users[201331242]=("Roopal","Bhopal","19.01")
 Users[201302001]=("xyz","fbc","27.02")
 Users[201308009]=("dvcdf","fdf","12.03")
 Users[201304008]=("dsad","dsds","02.04")
 Users[201304321]=("sfdsf","fdfd","03.08")
 Acquaintances={}
 Acquaintances[201301203]=[201302001,201302008,201302009]
 Acquaintances[201331242]=[201308009,201304008,201304321]
 Acquaintances[201308009]=[3432535435,54545435,454545435435]
 Acquaintances[201304008]=[34325234545,436346436,64643656,54366]
 Acquaintances[201304321]=[4343434343,3535454,45454545,454545435]
 Messages={}
 Messages[201301203]=[]
 Messages[201331242]=[]
 Messages[201302001]=[]
 Messages[201301205]=[]
"""


assert(q1.add_friend(201331242,201301203)==True)
assert(q1.add_friends(201301203,(201308009,201304321))==True)
assert(q1.remove_user(201304321)==True)
assert(q1.remove_user(201304008)==True)
assert(q1.remove_user(201304008)==False)
assert(q1.get_friends(201304008)==(34325234545, 436346436, 64643656, 54366))
assert(q1.get_friends_of_friends(201301203)==[23432432, 5345435, 5454545, 454543656, 34325234545, 436346436, 64643656, 54366, 3432535435, 54545435, 454545435435])
assert(q1.send_message(201301203, 201331242, "sgdsgfd")==True)
assert(q1.send_group_message(201331242, (201301203,201302001), "dfsdfdmsg")==True)
assert(q1.get_messages_from_friend(201301203,201331242)==((201331242, 3, 'dfsdfdmsg', '01/19/14', '23:28:57'),)
assert(q1.delete_message(201302001,4)==True)
assert(q1.delete_all_messages(201301203)==True)
assert(q1.delete_messages(201331242,[1,2])==2)
assert(q1.delete_messages_from_friend(201302001,201301203)==True)

