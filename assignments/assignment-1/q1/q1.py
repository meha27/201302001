import time
count=0
def increment():
	global count
	count=count+1
	return count

def add_friend(user_id, friend_id):
	flag1=0
	flag2=0
	for t in Acquaintances :
	 if t==user_id:
        	Acquaintances[user_id].append(friend_id)
	        flag1=1
	 if t==friend_id:
	        Acquaintances[friend_id].append(user_id)
	        flag2=1
        if (flag1==1 and flag2==1):
	   return True
        return False



def add_friends(user_id, list1):
	for t in Acquaintances :
	 if t== user_id:
	      Acquaintances[user_id].extend(list1)
	      return True
	return False


def remove_user(user_id):
	if Users.has_key(user_id)==True:
	   del Users[user_id]
	   return True
	else:
	   return False

def get_friends(user_id):
	for t in Acquaintances :
	 if t==user_id:
	    h=tuple(Acquaintances[user_id])
	    return h
	return None

def get_friends_of_friends(user_id):
      if Acquaintances.has_key(user_id)==False:
	   return False
      else:
	new_list=[]
	for t in Acquaintances[user_id]:
		for d in Acquaintances[t]:
		 new_list.append(d)
	l3 = [x for x in new_list if x not in Acquaintances[user_id]]
	return l3


def send_message(sender_id, receiver_id, msg):
	for t in Users:
	 if t==sender_id:
	    flag=1
	    break
	else:
	    flag=0
        if flag==0:
	   return False
	for g in Users:
	 if g==receiver_id:
	    
	    
	    date=time.strftime("%x")
	    timem=time.strftime("%X")
	    mssg=(sender_id,increment(),msg,date,timem)
	    Messages[receiver_id].append(mssg)

	    return True
       
def send_group_message(sender_id, list2, msg):
       if Users.has_key(sender_id)==False:
	   return False
       else: 
	
            for k in list2:
	       
	        
		 date=time.strftime("%x")
		 timem=time.strftime("%X")
		 mssg=(sender_id,increment(),msg,date,timem)
	         Messages[k].append(mssg)
	       
	    return True

def get_messages_from_friend(receiver_id, friend_id):
      if Users.has_key(receiver_id)==False and Users.has_key(friend_id)==False:
	   return False
      else:
	
	m_tuple=[]
	for t in Messages[receiver_id]:
		if t[0]==friend_id:
		   m_tuple.append(t)
	new=tuple(m_tuple)
	return new
		
def get_messages_from_all_friends(receiver_id):
	if Users.has_key(receiver_id)==False:
	   return False
	else:
          k=tuple(Messages[receiver_id])
          return k


def get_birth_day_messages(receiver_id):
	if Users.has_key(receiver_id)==False:
	   return False
	else:
	 m_list=[]
         t=Users[receiver_id][2]
	 k=Messages[receiver_id]
	 for i in k:
		if i[3]==t:
		   m_list.append(i)
	 tuple1=tuple(m_list)
	 return tuple1


def delete_message(user_id, msg_id):
      flag=1
      if Users.has_key(user_id)==False:
	 return False
      else:
	 for k in Messages[user_id]:
	    if k[1]==msg_id:
               flag=0
	       Messages[user_id].remove(k)
            else:
	       continue
	 if flag==1:
	    return "Msg_id not valid for the given user"
	 
	 return True
def delete_messages(user_id,tuple_m):
       x=0
       if Users.has_key(user_id)==False:
	   return 0
       else:
        
	for i in tuple_m: 
		 delete_message(user_id,i)
	         x=x+1
	 
	return x
  
def delete_all_messages(user_id):
	if Users.has_key(user_id)==False:
	   return False
	else:
         Messages[user_id]=()
         return True

def delete_messages_from_friend(receiver_id, friend_id):
	if Users.has_key(receiver_id)==True and Users.has_key(friend_id)==True:
	    for t in Messages[receiver_id]:
	        if t[0]==friend_id:
	           Messages[receiver_id].remove(t)
	    return True
	else:
		return False
	         

Users={}
Users[201301203]=("Rashi","Hyderabad","20.02")
Users[201331242]=("Roopal","Bhopal","19.01")
Users[201302001]=("xyz","fbc","27.02")
Users[201308009]=("dvcdf","fdf","12.03")
Users[201304008]=("dsad","dsds","02.04")
Users[201302001]=("dfdsf","fdfdgg","03.06")
Users[201302009]=("FDDF","fdfg","23.09")
Acquaintances={}
Acquaintances[201301203]=[201302001,201302008,201302009]
Acquaintances[201331242]=[201308009,201304008,201304321]
Acquaintances[201302009]=[3432535435,54545435,454545435435]
Acquaintances[201302008]=[34325234545,436346436,64643656,54366]
Acquaintances[201304321]=[4343434343,3535454,45454545,454545435]
Acquaintances[201302001]=[23432432,5345435,5454545,454543656]
Messages={}
Messages[201301203]=[]
Messages[201331242]=[]
Messages[201302001]=[]
Messages[201301205]=[]

