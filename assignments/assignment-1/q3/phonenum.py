
"""Phonenum - an abstraction to represent mobile phone numbers."""

"This type is meant to be used only in the classroom!"
def create_contacts():
	return {}

def add_contacts(contacts, name, phone_num, phone_type,countrycode=None):
	if contacts.has_key(name):
		phone_nums = contacts[name]
		phone_nums.append(Phonenum(phone_num, phone_type,countrycode))
	else:
		contacts[name] = [ Phonenum(phone_num, phone_type,countrycode) ]

__all__ = ["Phonenum", "phonenum_work", "phonenum_home", "phonenum_same"]

_phonenum_types_ = ["work", "home"]

def Phonenum(numstr, type,Countrycode=None):
    """constructs an instance of a phone-number."""
    num = numstr.strip()
    if len(num) is not 10:
        return None
    elif num.isdigit()==True:
	        if Countrycode==None:
                       return (num, type.strip().lower())
	        else: 
	               return (num,type.strip().lower(),Countrycode)
	       

def phonenum_work(tn):
    num, type = tn
    return type == "work"

def phonenum_home(tn):
    num, type = tn
    return type == "home"

def phonenum_same(this, that):
    return _phonenum_valid_(this) and _phonenum_valid_(that) and this[0] == that[0]

def _phonenum_valid_(tn):
    return isinstance(tn, tuple) and len(tn) == 2 and \
            isinstance(tn[0], str) and len(tn[0]) == 10

def update_contact_number(contacts, contact_name, old_number, new_number):
	 if contacts.has_key(contact_name):
	        for t in contacts[contact_name]:
	         for i in t:
		  if i == old_number:
		     k=list(t)
	             type=k[1]
		     contacts[contact_name].append(Phonenum(new_number,type))
		     contacts[contact_name].remove(t)
	          return True
         return False
if __name__=="__main__":

 assert(Phonenum("abcde12345","home","78")==None)
 assert(Phonenum("1234512345","home")==('1234512345', 'home'))
 assert(Phonenum("1234612345","home","78")==('1234612345', 'home', '78'))
 con=create_contacts()
 add_contacts(con,"xyz","1234523456","home","91")
 add_contacts(con,"abc","1234567890","work","1")
 update_contact_number(con,"abc","1234567890","0987654321")
 assert(con["abc"]==[('0987654321', 'work')])


