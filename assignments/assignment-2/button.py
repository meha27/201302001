from window import ChildWindow
from window import AppWindow
from point import Point
from window import BadArgumentError

class Button(ChildWindow):
    def __init__(self, parent, title, top_left, w, h):
        #assert(parent is not None)
        #assert(isinstance(parent, (AppWindow)))
        #assert(isinstance(top_left, Point))
        if parent is None or not isinstance(parent, Container):
        	raise BadArgumentError("Expecting a valid parent window")
        self.parent=parent        
        Window.__init__(self, parent, title, top_left, w, h)

    def click(self,option):
	    if isinstance(self.parent,Container):          #click function 
		    if option=="accept":                   #accept will set the option as accept(the one we defined earlier in dialogbox
		       self.parent.accept()                #cancel sets to cancel
		    elif option=="cancel":
		       self.parent.cancel()
		    else:
		       raise BadArgumentError("should be cancel or accept") #exception handling 
    	
