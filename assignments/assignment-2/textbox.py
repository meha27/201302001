from window import *
from point import Point
from dialogbox import *
from button import *

""" Unicode enabled text widget,"""
class TextBox(ChildWindow):
    txt=""
    call=0
    def __init__(self, parent, title, top_left, w, h):
        #assert(parent is not None)
        #assert(isinstance(parent, (AppWindow)))
        #assert(isinstance(top_left, Point))
        if parent is None or not isinstance(parent, Container):
        	raise BadArgumentError("Expecting a valid parent window")
        
        Window.__init__(self, parent, title, top_left, w, h)
#self.txt=txt

    def setText(self, text):
	TextBox.call=1
	txt.TextBox=text
    	

    def getText(self):
    	t=raw_input("enter text:")
	TextBox.setText(self,t)

    def validate(self, validator):
	
	          return validate(self.txt)         #returns the name of the fuction   
#raise BadArgumentError("none of number,float,URL")



def isaNumber(text):
	return isinstance(text,int) or isinstance(text,float) or isinstance(text,long)  #there are 3 types of numeric datatype in python
                                                        #checks if anyof the three
def isaFloat(text):
	return isinstance(text,float)

def isaURL(text):
	import urlparse
	
	parts = urlparse.urlsplit(text)
	if not parts.scheme or not parts.netloc:  
	    return 1
	else:
	    return 0

