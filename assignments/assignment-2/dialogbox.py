from window import *
from point import Point

class DialogBox(Container, ChildWindow):
    STATE_ACCEPT=0x1
    STATE_CANCEL=0x2
    def __init__(self, parent, title, top_left, w, h):
    	assert(parent is not None)
    	assert(isinstance(parent, (AppWindow)))
        assert(isinstance(top_left, Point))
        self.parent=parent
        Container.__init__(self, parent, title, top_left, w, h)

#STATE_ACCEPT=0x1
#	STATE_CANCEL=0X2

    def accept(self):
    	self.state=DialogBox.STATUS_ACCEPT     #setting the status of dialogbox 

    def cancel(self):                              #setting as cancel
    	self.state=DialogBox.STATUS_CANCEL
				  

    def getState(self):
	    return self.state
